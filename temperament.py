import numpy as np
from itertools import chain, combinations
from .jiTone import jiTone


def cents(x):
    return 1200*np.log(x)/np.log(2)


################################################################################


def powerset(iterable):
    """
    powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    """
    xs = list(iterable)
    return list(chain.from_iterable(combinations(xs,n) for n in range(len(xs)+1)))


def partial_powerset(iterable, n):
    ''' Only include sets of maximum size n. '''
    xs = list(iterable)
    return list(chain.from_iterable(combinations(xs,n) for n in range(n+1)))


def get_subgroup(*primes, **kwargs):
    depth = kwargs.get('depth', 1) # actually fixed for now
    max_factors = kwargs.get('max_factors', 2) # actually fixed for now
    subgroup = []

    # List of possible numerators and denominators
    prime_subsets = partial_powerset(primes, 2)
    products = [ int(np.prod(np.array(subset))) for subset in prime_subsets ]
    print(products)

    # List of fractions, turned into tones
    fractions = list(combinations(products, 2))
    tones  = [ jiTone(frac[0], frac[1], octred=True) for frac in fractions ]
    tones += [ jiTone(frac[1], frac[0], octred=True) for frac in fractions ]
    tones = sorted(tones)

    unique_tones = [tones[0]]
    for n in range(1,len(tones)):
        if(tones[n] > unique_tones[-1]):
            unique_tones += [tones[n]]

    for tone in unique_tones:
        print(tone, tone.cents())


