# We use an existing gcd method instead of writing our own.
# One of these should work, depending on the Python version.
try:
    from math import gcd
except:
    from fractions import gcd
    pass

import numpy as np
from . import johnston


class jiTone:

    def __init__(self, *args, **kwargs):
        ''' Takes two arguments, numerator and denominator of a JI fraction.
        The second argument is optional, and set to 1 if absent.
        kwargs:
        '''
        self.over = args[0]
        if(len(args)==1):
            self.under = 1
        else:
            self.under = args[1]
        self.reduce()
        if('octred' in kwargs and kwargs['octred']):
            self.octaveReduce()
        return


    def value(self):
        ''' Returns floating point value of tone. '''
        return float(self.over)/float(self.under)

    def cents(self):
        ''' The cent value of the tone. '''
        return 1200*np.log(self.over/self.under)/np.log(2)

    def johnston(self):
        ''' A Johnston notation representation. '''
        return johnston.note_name(self.over,self.under)

    def display(self):
        ''' Displays the tone in a formatted way on one line. '''
        frac = "{:d}/{:d}".format(self.over, self.under)
        jstn = self.johnston()
        print( frac.rjust(8), "|", jstn.ljust(8) )
        return


    def reduce(self):
        ''' Reduces to coprime form. '''
        divisor = gcd(self.over,self.under)
        self.over //= divisor
        self.under //= divisor
        return

    def octaveReduce(self):
        ''' Reduces tone to fall within an octave of 1/1. '''
        while( self.value() < 1 ):
            self.over *= 2
        while( self.value() > 2 ):
            self.under *= 2
        self.reduce()
        return

    def tritoneReduce(self):
        ''' Compares octave-reduced self to its octave-reduced inversion,
        and makes the tone equal to the lesser of these.
        '''
        self.octaveReduce()
        inversion = jiTone(1,1) / self
        inversion.octaveReduce()
        if(inversion < self):
            self.over  = inversion.over
            self.under = inversion.under
        return


    def __str__(self):
        ''' Prints the tone as a fraction. '''
        return "{:d}/{:d}".format(self.over, self.under)

    def __mul__(self, factor):
        ''' Compounds two tones, i.e., multiplies their fractions. '''
        newNum = self.over*factor.over
        newDenom = self.under*factor.under
        return jiTone(newNum,newDenom)

    def __add__(self, factor):
        ''' Compounds two tones, i.e., is identical to __mul__. '''
        return self.__mul__(factor)

    def __truediv__(self, factor):
        ''' Inversely compounds one tone by second, i.e., divides them. '''
        newNum = self.over*factor.under
        newDenom = self.under*factor.over
        return jiTone(newNum,newDenom)

    def __div__(self, factor):
        ''' Is the same as __truediv__. '''
        return self.__truediv__(factor)

    def __sub__(self, factor):
        ''' Is the same as __truediv__. '''
        return self.__truediv__(factor)


    def __eq__(self, other):
        return (self.over*other.under == self.under*other.over)

    def __ne__(self, other):
        return (self.over*other.under != self.under*other.over)

    def __lt__(self, other):
        return (self.over*other.under < self.under*other.over)

    def __gt__(self, other):
        return (self.over*other.under > self.under*other.over)
